+++
title = "P1 Finite Element Example"
+++

### P1 Finite Element Method
All following pictures show the results of the same finite element application running on different grids. All visualization is done using the vtk/paraview export of Dune. A detailed problem description can be found in:

P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, R. Kornhuber, M. Ohlberger, O. Sander. **A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part II: Implementation and Tests in DUNE.** [Computing 82(2-3):121-138, 2008](http://www.springerlink.com/content/gn177r643q2168g7/), [Preprint](http://www.matheon.de/research/show_preprint.asp?action=details&serial=404).


1. 2D simulation using Dune::Grid::Alberta
<img src="/img/alberta2d.jpg" alt="" class="nofloat" width="600" height="600">

2. 3D simulation using Dune::Grid::Alberta
<img src="/img/alberta3d.jpg" alt="" class="nofloat" width="600" height="600">

3. 3D simulation using Dune::Grid::ALU3dGrid with cubes
<img src="/img/alucube3d.jpg" alt="" class="nofloat" width="600" height="600">

4. 3D simulation using Dune::Grid::ALU3dGrid with simplices
<img src="/img/alusimplex3d.jpg" alt="" class="nofloat" width="600" height="600">

5. Isosurface of a 3D simulation
<img src="/img/iso.jpg" alt="" class="nofloat" width="600" height="600">

6. 2D simulation using Dune::Grid::UGGrid with cubes
<img src="/img/ugcube2d.jpg" alt="" class="nofloat" width="600" height="600">

7. 3D simulation using Dune::Grid::UGGrid with cubes
<img src="/img/ugcube3d.jpg" alt="" class="nofloat" width="600" height="600">

8. 3D simulation using Dune::Grid::UGGrid with simplices
<img src="/img/ugsimplex2d.jpg" alt="" class="nofloat" width="600" height="600">

9. 3D simulation using Dune::Grid::UGGrid with simplices
<img src="/img/ugsimplex3d.jpg" alt="" class="nofloat" width="600" height="600">

10. 3D simulation using Dune::Grid::YaspGrid
<img src="/img/yasp3d.jpg" alt="" class="nofloat" width="600" height="600">
