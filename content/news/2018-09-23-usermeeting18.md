+++
date = "2018-09-23T22:14:00+01:00"
title = "Invitation to the Dune User and Developer Meeting 2018"
+++

We are going to organize a user meeting in Stuttgart,
details still have to be fixed (e.g. exact venue and
the format) but the one thing that is decided on, is that
we will start on the 5th of November and will continue on
the 6th.  A Dune developer meeting will be held at the same venue
right after the user meeting ending on the 8th.

Details will be made available on the
[following webpage][webpage] as soon as possible - but do
block the week 5th to 8th of November now!

[webpage]: https://www.ians.uni-stuttgart.de/institute/news/events/dune-user-and-developer-meeting-2018
