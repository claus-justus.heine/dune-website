+++
date = "2012-06-04"
title = "Dune 2.2.0 Released"
+++

We are proud the announce the release of the new stable version 2.2 of the Dune core modules. Dune 2.2 brings you few big changes, but many small improvements and lots of API cleanup. You can read the [release notes]($(ROOT)/releasenotes/releasenotes-2.2.html) for all the details. Tarballs and svn access is available on our [download]($(ROOT)/download.html) page. We hope you enjoy this new release, and recommend it to your friends and family. If you encounter any problems please let us now by way of the [mailing list]($(ROOT)/mailinglists.html) or the [bugtracker](http://www.dune-project.org/flyspray/).
