+++
date = "2015-08-26"
title = "Dune 2.4.0-rc2 Released"
+++

The second release candidate for the upcoming 2.4 release of the core modules is now available. You can download the tarballs from our [download](/releases/) page, checkout the v2.4.0-rc2 tag of the modules via git. For a list of changes and known issues please refer to the [release notes](/releases/2.4.0). Please go and test, and report the problems that you encounter.
