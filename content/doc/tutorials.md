+++
title = "Tutorials"
[menu.main]
parent = "docs"
weight = 3
+++
### Using DUNE

If you are new to Dune, check out the text on
[how to get started](http://www.math.tu-dresden.de/~osander/research/sander-getting-started-with-dune.pdf).


#### Installation and the build system
+ [Installation Notes](/doc/installation) for the DUNE core modules.
+ Documentation on the [CMake-based build system](/sphinx/core/).

#### dune-common
+ A description of the parallel communication interface is contained in the documentation directory of [dune-common](https://gitlab.dune-project.org/core/dune-common) and will be built as part of the module if you have LaTeX installed.

#### dune-geometry
+ A description of virtual refinement is contained in the documentation directory of [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry) and will be built as part of the module if you have LaTeX installed.

#### dune-grid
+ A tutorial on the Dune grid interface is available in the [dune-grid-howto](https://gitlab.dune-project.org/core/dune-grid-howto) module and will be built as part of the module if you have LaTeX installed.
+ The [view concept](/doc/view-concept) is the fundamental idea of the grid interface

#### dune-istl
+ A tutorial on the ISTL solver library is contained in the documentation directory of [dune-istl](https://gitlab.dune-project.org/core/dune-istl) and will be built as part of the module if you have LaTeX installed.
