+++
date = "2009-04-02T16:03:35+01:00"
major_version = 1
minor_version = 2
modules = ["dune-common", "dune-istl", "dune-grid", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 0
title = "Dune 1.2.0"
version = "1.2.0"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 1.2 - Release Notes

**Warning:** Many things have been marked deprecated. However note that class deprecation is buggy in all versions of gcc prior to 4.3.

## dune-common

#### buildsystem

*   Module name, version and dependencies are automatically extracted from the `dune.module` file. Please remove the `AC_INIT(...)` entry from `configure.ac` of your modules and use `DUNE_AC_INIT` instead.
*   The update-libs mechanism is removed from dune-common, as its use was really limited and it introduced certain problems. Please update your Makefile.am files if you were using this make trick. All
    `foo_DEPENDENCIES = $(LOCAL_LIBS)`
    entries must be removed.
*   All modules should specify a version. The version is of the form <span style="font-style: italic">M.m.r</span>, with major version <span style="font-style: italic">M</span>, minor version <span style="font-style: italic">m</span> and revision <span style="font-style: italic">r</span>. In `common/version.hh` macros are provided to test on a certain version version of a module during the preprocessor run.
*   The is now a [document](../doc/buildsystem/buildsystem.pdf) describing the build system. It is distributed as part of dune-common.

#### application programming interface

*   The class `FixedArray`, deprecated in DUNE 1.1, has been removed. Please use `array` instead.
*   The class `Array`, deprecated in DUNE 1.1, has been removed.
*   The class `RemoveConst`, deprecated in DUNE 1.1, has been removed. Please use `remove_const` instead, which conforms with the STL implementation.
*   The class `SameType`, deprecated in DUNE 1.1, has been removed. Please use `same_type` instead, which conforms with the STL implementation.
*   The classes `CompileTimeChecker` and `IsTrue` are deprecated. For static assertions use the new macro `dune_static_assert` which mimics the upcoming new static_assert language feature.
*   The class `Stack` has been deprecated. Please use `std::stack` instead.
*   The class `FiniteStack` has been moved to its own header `finitestack.hh`.
*   A new class `BitSetVector` has been added to `dune-common`. It provides an efficient implementation of a dynamic array of static arrays of booleans. It supersedes `BitField`, which has been deprecated.

## dune-grid

*   Support for isoparametric elements has been removed from `UGGrid`. The feature will reappear eventually in form of a meta grid.
*   The constructor of `UGGrid` taking two integer arguments (heapSize and envHeapSize) had been deprecated in Dune 1.1, as the environment heap size is now handled automatically by `UG`. It is now entirely removed.
*   `UGGrid` now numbers its level 0 vertices in the order they were inserted into the grid factory. This allows to import vertex data from files.
*   The `dimworld` template parameter has been removed from `YaspGrid`. It was there for historical reasons only. For a transition period the second parameter is still there, but with a default value. That way old code still compiles.
*   `AlbertaGrid` now supports ALBERTA 2.0.
*   Since ALBERTA supports only a fixed dimension of world (which can be changed by a preprocessor macro), the `dimworld` template parameter is being removed from `AlbertaGrid`, too. For compatiblity, the template parameter still exists and defaults to the only correct value.
*   IndexSets now use `unsigned int` instead of `int` to return indices. The type is exported as `IndexType` and may become grid-implementation-specific in the future.
*   GridParts have been deprecated. They are replaced by grid dependent GridViews providing basically the same functionality. As a consequence, IndexSets do not provide iterators anymore (though this can be reenabled for a transition phase).
*   The methods `wasRefined()` and `mightBeCoarsened()` have been renamed to `isNew()` and `mightVanish()`, respectively. We think that the new names capture the semantics of the methods better. The old methods are still there, but they are deprecated.
*   A generic implementation for geometries and reference elements has been added. Sadly though, the generic reference elements do not match the current reference elements and renumbering is required for now.
*   The method `operator[]` of the class `Geometry` has been marked deprecated. It is replaced by the method `corner`. Note that `corner` returns a **copy** of the corner coordinates instead of a reference to them. Moreover, `corner` numbers the corners in the same way as the generic reference element. This numbering differs for pyramids and prisms.
*   The class `Intersection` also received a method `type`, returning the `GeometryType` of the intersection.

## dune-istl

*   Finally dune-istl has an implementation of the restarted GMRES.
*   AMG works for sparse block matrices with an unsymmetric sparsity pattern.
*   New Constructor for RemoteIndices which takes a set of prospective ids of processors we might need to exchange information with. If specified this will prevent exchanging information with all processors in a ring (O(P) complexity in RemoteIdices::rebuild) and therefore speedup the rebuild for large processor numbers.
*   Parallel solvers tested on IBMs Blue Gene / P with up to 4096 processors.
*   A block diagonal matrix (DiagonalMatrix) was added.

## dune-grid-dev-howto

This new module has been added to the list of core modules. It is intended to contain a tutorial on how to implement grid implementations. No text exists yet, however, there is an example grid implementation. It is called `IdentityGrid` and wraps a given DUNE grid. It hence does not have any functionality itself but it contains all the boilerplate code you need for a grid implementation.
