+++
title = "Dune User Meeting 2017"
+++

# Dune User Meeting 2017 in Heidelberg

## Invitation

We invite all Dune users to participate at the 4th Dune User meeting, to
be held at Heidelberg from 13.03.2017 to 14.03.2017.

This meeting should be used to showcase how you're using DUNE,
foster future collaborations between the users and provide input to the future development of
DUNE. We keep the format rather informal. All participants are encouraged
to give a presentation. In addition, we
will reserve plenty of time for more general discussions on Dune.

## Date

The meeting is held on March 13-14, 2017. It starts on Monday 10am and ends on Tuesday afternoon.

The [Dune Developer Meeting 2017](https://www.dune-project.org/community/meetings/2017-03-devmeeting/)
takes place afterwards on March 15, 2017. Users are welcome to
join the discussions.

## Location

### Meeting Venue

The meeting will take place on the 5th floor of the Mathematikon, Im Neuenheimer Feld 205, 69120 Heidelberg.

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=8.67336004972458%2C49.41660264176612%2C8.676900565624239%2C49.41840336851726&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/#map=19/49.41750/8.67513">View Larger Map</a></small>


## Schedule

### Monday, March 13, 2017

| Time        | Speaker          | Description / Talk Title
|-------------|-----------------:|-------------------------
| 10:00-10:10 | Peter Bastian    | Welcome and short introduction
| 10:10-11:10 |                  | User presentations, Chair: Christian Engwer
|             | Simon Praetories | [Dune-DEC, a Dune Discrete Exterior Calculus module](/pdf/meetings/2017-03-usermeeting/dune-dec.pdf)
|             | Tobias Leibner   | *Moment models for kinetic equations in dune-gdt*
|             | Lasse Hinrichsen | *Higher-Order DG methods for Nonsmooth problems*
| 11:10-11:30 |                  | Coffee Break
| 11:30-12:30 |                  | User presentations, Chair: Peter Bastian
|             | Lukas Riedel     | *DORiE - from numeric routine to simulation suite*
|             | Leopold Stadler  | *DUNE-SWF: A module for shallow water flow and sediment transport*
|             | Birane Kane      | *Adapative DG for Flow in Porous Media*
| 12:30-14:00 |                  | Lunch break at *Bräustadel*
| 14:00-15:30 |                  | DuMuX user presentations, Chair: Timo Koch
|             | Simon Scholz     | *Modeling microbially enhanced coal-bed methane production with DuMuX*
|             | Kilian Weishaupt | *Coupling of free flow and proous media flow on multiple scales*
|             | Timo Koch        | *Mixed-dimension models in DuMuX - applications using two Dune grids*
|             |                  | *Feedback from the DuMuX community*
| 15:30-16:00 |                  | Coffee Break
| 16:00-17:20 |                  | User presentations, Chair: Andreas Dedner
|             | Anne Reinarz     | *Multiscale modelling of aerospace composites*
|             | Tomas Stary      | *Simulating the manufacturing of carbon fiber composites*
|             | Anhad Sandhu     | *Modelling cracaks using linear elasticity*
|             | Ole Klein        | *TBA*
| 17:20-17:40 |                  | Brainstorming for Tuesdays Blitztalk session
| 19:30       |                  | Conference Dinner (IWR Common Room)

### Tuesday, March 14, 2017

| Time        | Speaker          | Description / Talk Title
|-------------|------------------|-------------------------
|  9:00-10:30 |                  | User presentations, Chair: Steffen Müthing
|             | Martin Nolte     | *dune-corepy: Generating Python bindings for Dune*
|             | Andreas Dedner   | *dunepy: A general approach for exposing the Dune interface classes to python*
|             | Dominic Kempf    | *Generating performance-optimized finite element assembly kernels for dune-pdelab*
|             |                  | General discussion: *Python and Dune*
| 10:30-10:50 |                  | Coffee Break
| 10:50-11:30 |                  | User presentations, Chair: Dominic Kempf
|             | Marian Piatkowski| *A high-performance incompressible Navier-Stokes DG solver*
|             | Ansgar Burchardt | [Using GitLab CI for Dune modules](/pdf/meetings/2017-03-usermeeting/ansgar_gitlabci.pdf)
| 11:30-12:30 |                  | Blitztalks + General discussion session
| 12:30-14:00 |                  | Lunch Break at *Bräustadel*
| 14:00       |                  | Dune Developer Meeting

## Participants

| Name                 | Affiliation
|----------------------|----------------------------
| Peter Bastian        | IWR, Heidelberg University
| Markus Blatt         | Dr. Blatt - HPC-Simulation-Software & Services
| Ansgar Burchardt     | TU Dresden
| Andreas Dedner       | University of Warwick
| Christian Engwer     | University of Münster
| Jö Fahlke            | University of Münster
| Artur Hahn           | Neuroradiology, University Hospital Heidelberg
| Claus-Justus Heine   | IANS / University of Stuttgart
| René Heß             | IWR, Heidelberg University
| Lasse Hinrichsen     | FU Berlin
| Birane Kane          | University of Stuttgart
| Dominic Kempf        | IWR, Heidelberg University
| Ole Klein            | IWR, Heidelberg University
| Timo Koch            | IWS, University of Stuttgart
| Tobias Leibner       | Institute for Computational and Applied Mathematics Münster
| Steffen Müthing      | IWR, Heidelberg University
| Martin Nolte         | University of Freiburg
| Marian Piatkowski    | IWR, Heidelberg University
| Simon Praetorius     | Technische Universität Dresden, Institut for Scientific Computing
| Anne Reinarz         | University of Bath
| Lukas Riedel         | Institute of Environmental Physics, Heidelberg University
| Anhad Sandhu         | University of Exeter
| Simon Scholz         | IWS, University of Stuttgart
| Leopold Stadler      | Bundesanstalt für Wasserbau
| Tomas Stary          | University of Exeter
| Kilian Weishaupt     | IWS, University of Stuttgart

## Organizers

The Dune user meeting 2017 is organized by

* [Dominic Kempf](mailto:dominic.kempf@iwr.uni-heidelberg.de)
* [Timo Koch](mailto:timo.koch@iws.uni-stuttgart.de)
* [Steffen Müthing](mailto:steffen.muething@iwr.uni-heidelberg.de)

Please do not hesitate to contact us, if you have any questions or suggestions
concerning the meeting.
